RESTful API for [MI-RI](https://bitbucket.org/ntrrg/mi_ri) written in [Go](https://golang.org).

## Acknowledgment

Working on this project I use/used:

* [Debian](https://www.debian.org/)

* [XFCE](https://xfce.org/)

* [st](https://st.suckless.org/)

* [Zsh](http://www.zsh.org/)

* [GNU Screen](https://www.gnu.org/software/screen)

* [Git](https://git-scm.com/)

* [EditorConfig](http://editorconfig.org/)

* [Vim](https://www.vim.org/)

* [GNU make](https://www.gnu.org/software/make/)

* [Chrome](https://www.google.com/chrome/browser/desktop/index.html)

* [Bitbucket](https://bitbucket.org)

* [Docker](https://docker.com)

* [ntgo](https://github.com/ntrrg/ntgo)

* [Vestigo](https://github.com/husobee/vestigo)

