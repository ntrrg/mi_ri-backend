gofiles := $(shell find . -iname "*.go" -type f)
gofiles := $(filter-out ./vendor/%, $(gofiles))
gofiles := $(filter-out ./external/%, $(gofiles))
gosrcfiles := $(filter-out %_test.go, $(gofiles))
pkg_name := mi_ri-backend
make_bin := /tmp/$(pkg_name)-bin

.PHONY: all
all: build

.PHONY: build
build: dist/$(pkg_name)

.PHONY: build-docker
build-docker:
	docker build -t ntrrg/$(pkg_name) .

.PHONY: build-docker-debug
build-docker-debug:
	docker build --target debug -t ntrrg/$(pkg_name):debug .

.PHONY: clean
clean:
	rm -rf dist/

.PHONY: docs
docs:
	@echo "In progress.."

dist/$(pkg_name): $(gosrcfiles)
	CGO_ENABLED=0 go build -o $@

# Development

coverage_file := coverage.txt

.PHONY: benchmark
benchmark:
	go test -bench . -benchmem ./...

.PHONY: ci
ci: test lint qa coverage benchmark build

.PHONY: clean-dev
clean-dev: clean
	rm -rf $(make_bin)
	rm -rf $(coverage_file)
	docker image rm ntrrg/$(pkg_name) || true

.PHONY: coverage
coverage: $(coverage_file)
	go tool cover -func $<

.PHONY: coverage-web
coverage-web: $(coverage_file)
	go tool cover -html $<

.PHONY: deps-dev
deps-dev: $(make_bin)/gometalinter
	go get -u -v golang.org/x/lint/golint

.PHONY: format
format:
	gofmt -s -w -l $(gofiles)

.PHONY: lint
lint:
	gofmt -d -e -s $(gofiles)
	golint ./ ./api/...

.PHONY: qa
qa: $(make_bin)/gometalinter
	PATH="$(make_bin):$$PATH" CGO_ENABLED=0 gometalinter --tests ./ ./api/... | grep -v "golang.org/x/oauth2@" | grep -v "golang.org/x/crypto@" || true

.PHONY: test
test:
	go test -v ./...

$(coverage_file): $(gofiles)
	go test -coverprofile $@ -v ./...

$(make_bin)/gometalinter:
	mkdir -p $(make_bin)
	cd /tmp && wget 'https://github.com/alecthomas/gometalinter/releases/download/v2.0.11/gometalinter-2.0.11-linux-amd64.tar.gz'
	tar -xf /tmp/gometalinter-2.0.11-linux-amd64.tar.gz -C /tmp/
	cp -a $$(find /tmp/gometalinter-2.0.11-linux-amd64/ -type f) $(make_bin)/

