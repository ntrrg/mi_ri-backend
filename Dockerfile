FROM golang:1.11.2-alpine3.8 AS build
RUN apk update && apk add git make
WORKDIR /src
COPY . .
RUN make

FROM alpine:3.8 as debug
COPY --from=build /src/dist /bin

FROM scratch
COPY --from=build /src/dist /bin
EXPOSE 4000
USER 1000
ENTRYPOINT ["/bin/mi_ri-backend"]
CMD ["--db", "$DATABASE", "--gcid", "$GCID", "--gcs", "$GCS", "--grurl", "$GRURL", "-smtphost", "$SMTP_HOST", "--smtpport", "$SMTP_PORT", "--smtpemail", "$SMTP_EMAIL", "--smtppasswd", "$SMTP_PASSWORD"]

