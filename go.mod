module bitbucket.org/ntrrg/mi_ri-backend

require (
	cloud.google.com/go v0.34.0 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/gbrlsnchs/jwt/v2 v2.0.0
	github.com/go-sql-driver/mysql v1.4.1
	github.com/husobee/vestigo v1.1.0
	github.com/ntrrg/ntgo v0.1.0
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/stretchr/testify v1.2.2 // indirect
	golang.org/x/crypto v0.0.0-20181203042331-505ab145d0a9
	golang.org/x/oauth2 v0.0.0-20181203162652-d668ce993890
	golang.org/x/sync v0.0.0-20181108010431-42b317875d0f // indirect
	google.golang.org/appengine v1.3.0 // indirect
)
