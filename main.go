// Copyright 2018 Miguel Angel Rivera Notararigo. All rights reserved.
// This source code was released under the MIT license.

package main

import (
	"flag"
	"log"
	"net/http"
	"os"
	"strings"

	nthttp "github.com/ntrrg/ntgo/net/http"

	"bitbucket.org/ntrrg/mi_ri-backend/api/rest"
)

var (
	restOpts    nthttp.Config
	key, cert   string
	adminPasswd string
	database    string
	verbose     bool
	debug       bool
	logfile     string
	baseurl     string
	jwtSecret   string

	googleClientID     string
	googleClientSecret string
	googleRedirectURL  string

	smtpHost     string
	smtpPort     string
	smtpEmail    string
	smtpPassword string
)

func main() {
	config()
	server := nthttp.NewServer(&restOpts)
	var err error

	lVerbose("[INFO][SERVER] Listening on %v..\n", restOpts.Addr)

	if strings.Contains(restOpts.Addr, "/") {
		err = server.ListenAndServeUDS()
	} else if key != "" && cert != "" {
		err = server.ListenAndServeTLS(cert, key)
	} else {
		err = server.ListenAndServe()
	}

	if err != http.ErrServerClosed {
		log.Fatalf("[FATAL][SERVER] Can't start the server -> %v", err)
	}

	<-server.Done
	rest.CloseDB()
}

func config() {
	flag.StringVar(
		&restOpts.Addr,
		"addr",
		":4000",
		"TCP address to listen on. If a path to a file is given, the server will "+
			"use a Unix Domain Socket.",
	)

	flag.StringVar(&baseurl, "url", "http://localhost:4000", "Base URL")
	flag.StringVar(&key, "key", "", "TLS private key file")
	flag.StringVar(&cert, "cert", "", "TLS certificate file")
	flag.StringVar(&adminPasswd, "passwd", "admin", "Administrator password")
	flag.StringVar(&database, "db", "root:root@/miri", "Database server")
	flag.BoolVar(&verbose, "verbose", true, "Enable verbose messages")
	flag.BoolVar(&debug, "debug", false, "Enable debugging")
	flag.StringVar(&logfile, "log", "", "Log file location (default: stderr)")
	flag.StringVar(&googleClientID, "gcid", "", "Google Client ID")
	flag.StringVar(&googleClientSecret, "gcs", "", "Google Client Secret")
	flag.StringVar(&jwtSecret, "jwt", "jwtsecret", "JWT secret signing")
	flag.StringVar(&smtpHost, "smtphost", "smtp.google.com", "SMTP ost")
	flag.StringVar(&smtpPort, "smtpport", "587", "SMTP port")
	flag.StringVar(&smtpEmail, "smtpemail", "", "SMTP email")
	flag.StringVar(&smtpPassword, "smtppasswd", "", "SMTP password")

	flag.StringVar(
		&googleRedirectURL,
		"grurl",
		"http://localhost:4000/v1/oauth/google",
		"Google redirect URL",
	)

	flag.Parse()

	if debug {
		verbose = true
	}

	if logfile != "" {
		lf, err := os.OpenFile(logfile, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0600)
		if err != nil {
			log.Fatalf("[FATAL][SERVER] Can't create/open the log file -> %v", err)
		}

		defer func() {
			if err := lf.Close(); err != nil {
				log.Printf("[ERROR][SERVER] Can't close the log file -> %v", err)
			}
		}()

		log.SetOutput(lf)
	}

	if err := rest.OpenDB(database); err != nil {
		log.Fatalf("[FATAL][DB] Can't create the connection pool -> %v", err)
	}

	rest.AdminPasswd = adminPasswd
	rest.BaseURL = baseurl
	rest.JWTSecret = jwtSecret
	rest.SMTPHost = smtpHost
	rest.SMTPPort = smtpPort
	rest.SMTPEmail = smtpEmail
	rest.SMTPPassword = smtpPassword
	rest.GoogleOAuthConfig.ClientID = googleClientID
	rest.GoogleOAuthConfig.ClientSecret = googleClientSecret
	rest.GoogleOAuthConfig.RedirectURL = googleRedirectURL
	rest.SetVerbose(verbose)
	rest.SetDebug(debug)
	restOpts.Handler = rest.Mux()
}

func lVerbose(format string, v ...interface{}) {
	if verbose {
		log.Printf(format, v...)
	}
}
