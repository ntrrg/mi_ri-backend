// Copyright 2018 Miguel Angel Rivera Notararigo. All rights reserved.
// This source code was released under the MIT license.

package rest

import (
	"database/sql"
	"log"
	"strings"

	_ "github.com/go-sql-driver/mysql" // MySQL driver
)

const schema = `
DROP TABLE IF EXISTS people, users;

CREATE TABLE users (
  id BIGINT UNSIGNED AUTO_INCREMENT,
  email VARCHAR(320) NOT NULL,
  passwd VARCHAR(72) NOT NULL DEFAULT "",
  mode VARCHAR(20) NOT NULL DEFAULT "local",
  created_at INTEGER NOT NULL,
  last_login INTEGER DEFAULT 0,
	UNIQUE(email),
  PRIMARY KEY(id)
) ENGINE=InnoDB;

CREATE TABLE people (
  id BIGINT UNSIGNED NOT NULL,
  name VARCHAR(100) NOT NULL DEFAULT "",
  phone VARCHAR(20) NOT NULL DEFAULT "",
  address VARCHAR(300) NOT NULL DEFAULT "",
  PRIMARY KEY(id),
  FOREIGN KEY(id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE=InnoDB;
`

var db *sql.DB

// CloseDB closes the connection pool.
func CloseDB() {
	if err := db.Close(); err != nil {
		log.Printf(errCloseDBPool, err)
	}
}

// OpenDB creates the connection pool.
func OpenDB(v string) error {
	var err error
	db, err = sql.Open("mysql", v)
	if err != nil {
		log.Printf(errCreateDBPool, err)
	}

	return err
}

func createDB() error {
	tx, err := newTx()
	if err != nil {
		return err
	}

	defer txRollback(tx)

	for _, q := range strings.Split(schema, ";\n") {
		q = strings.Trim(q, " \t\n")

		if q == "" {
			continue
		}

		if _, err := txExec(tx, q); err != nil {
			return err
		}
	}

	return txCommit(tx)
}

func pingDB() error {
	err := db.Ping()
	if err != nil {
		log.Printf(errConnectDBMS, err)
	}

	return err
}

func newTx() (*sql.Tx, error) {
	tx, err := db.Begin()
	if err != nil {
		log.Printf(errStartTx, err)
	}

	return tx, err
}

func txCommit(tx *sql.Tx) error {
	err := tx.Commit()
	if err != nil {
		log.Printf(errCommitTx, err)
	}

	return err
}

func txExec(tx *sql.Tx, q string, args ...interface{}) (sql.Result, error) {
	result, err := tx.Exec(q, args...)
	q = strings.Trim(q, " \t\n")
	lDebug(debRunQuery, q, args)

	if err != nil {
		log.Printf(errRunQuery, err)
	}

	return result, err
}

func txQueryRow(tx *sql.Tx, q string, args ...interface{}) *sql.Row {
	row := tx.QueryRow(q, args...)
	q = strings.Trim(q, " \t\n")
	lDebug(debRunQuery, q, args)
	return row
}

func txQuery(tx *sql.Tx, q string, args ...interface{}) (*sql.Rows, error) {
	rows, err := tx.Query(q, args...)
	q = strings.Trim(q, " \t\n")
	lDebug(debRunQuery, q, args)

	if err != nil {
		log.Printf(errRunQuery, err)
	}

	return rows, err
}

func txRollback(tx *sql.Tx) {
	if err := tx.Rollback(); err != nil && err != sql.ErrTxDone {
		log.Printf(errRollbackTx, err)
	}
}
