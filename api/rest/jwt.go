// Copyright 2018 Miguel Angel Rivera Notararigo. All rights reserved.
// This source code was released under the MIT license.

package rest

import (
	"log"
	"time"

	"github.com/gbrlsnchs/jwt/v2"
)

// Token is a JWT token with public claims.
type Token struct {
	*jwt.JWT
	ID string `json:"id"`
}

// NewJWT generates a new JWT for the given user.
func NewJWT(id string) (string, error) {
	hs256 := jwt.NewHS256(JWTSecret)

	jot := &Token{
		JWT: &jwt.JWT{
			Issuer:   "MI_RI",
			Audience: "owner",
			IssuedAt: time.Now().Unix(),
		},
		ID: id,
	}

	jot.SetAlgorithm(hs256)
	payload, err := jwt.Marshal(jot)
	if err != nil {
		log.Printf("[ERROR][JWT] Can't encode the JWT -> %v", err)
		return "", err
	}

	token, err := hs256.Sign(payload)
	if err != nil {
		log.Printf("[ERROR][JWT] Can't sign the JWT -> %v", err)
		return "", err
	}

	return string(token), nil
}

// VerifyJWT checks the token signature.
func VerifyJWT(token string) error {
	hs256 := jwt.NewHS256(JWTSecret)

	payload, sig, err := jwt.Parse(token)
	if err != nil {
		log.Printf("[ERROR][JWT] Can't decode the JWT -> %v", err)
		return err
	}

	if err := hs256.Verify(payload, sig); err != nil {
		log.Printf("[ERROR][JWT] The JWT is not valid -> %v", err)
		return err
	}

	jot := &Token{}
	if err = jwt.Unmarshal(payload, jot); err != nil {
		return err
	}

	issValidator := jwt.IssuerValidator("MI_RI")
	audValidator := jwt.AudienceValidator("owner")
	err = jot.Validate(issValidator, audValidator)
	if err != nil {
		log.Printf("[ERROR][JWT] The JWT is not valid -> %v", err)
	}

	return err
}

// NewRecoverLinkKey generates a key for recovery links.
func NewRecoverLinkKey(email string) (string, error) {
	hs256 := jwt.NewHS256(SMTPPassword)

	jot := &Token{
		JWT: &jwt.JWT{
			Issuer:   "MI_RI",
			IssuedAt: time.Now().Unix(),
		},
		ID: email,
	}

	jot.SetAlgorithm(hs256)
	payload, err := jwt.Marshal(jot)
	if err != nil {
		log.Printf("[ERROR][SMTP] Can't encode the link key -> %v", err)
		return "", err
	}

	token, err := hs256.Sign(payload)
	if err != nil {
		log.Printf("[ERROR][SMTP] Can't sign the link key -> %v", err)
		return "", err
	}

	return string(token), nil
}

// VerifyRecoverLinkKey checks the link key signature.
func VerifyRecoverLinkKey(key string) error {
	hs256 := jwt.NewHS256(SMTPPassword)

	payload, sig, err := jwt.Parse(key)
	if err != nil {
		log.Printf("[ERROR][JWT] Can't decode the link key -> %v", err)
		return err
	}

	if err := hs256.Verify(payload, sig); err != nil {
		log.Printf("[ERROR][JWT] The link key is not valid -> %v", err)
		return err
	}

	return err
}
