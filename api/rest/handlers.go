// Copyright 2018 Miguel Angel Rivera Notararigo. All rights reserved.
// This source code was released under the MIT license.

package rest

import (
	"context"
	"crypto/rand"
	"database/sql"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/smtp"
	"net/url"
	"strings"
	"time"

	"github.com/gbrlsnchs/jwt/v2"
	"github.com/husobee/vestigo"
	"golang.org/x/crypto/bcrypt"
)

// Healthz is the handler for the healtz endpoint.
func Healthz(w http.ResponseWriter, r *http.Request) {
	if err := pingDB(); err != nil {
		ErrDBMSNotReady.ServeHTTP(w, r)
		return
	}

	tx, err := newTxHTTP(w, r)
	if err != nil {
		return
	}

	defer txRollback(tx)

	q := "SELECT id, email, passwd, created_at, last_login FROM users LIMIT 1"
	if _, err := txQueryHTTP(w, r, tx, q); err != nil {
		return
	}
}

// Reset resets the database.
func Reset(w http.ResponseWriter, r *http.Request) {
	if err := createDB(); err != nil {
		ErrInternal.ServeHTTP(w, r)
		return
	}
}

// GetToken generates a new token.
func GetToken(w http.ResponseWriter, r *http.Request) {
	email, password, ok := r.BasicAuth()

	if !ok {
		unauthorized(w, r)
		return
	}

	tx, err := newTxHTTP(w, r)
	if err != nil {
		return
	}

	defer txRollback(tx)

	var id, hash string
	q := "SELECT id, passwd FROM users WHERE email=?"
	err = txQueryRow(tx, q, email).Scan(&id, &hash)
	if err != nil {
		unauthorized(w, r)
		return
	}

	if err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password)); err != nil {
		unauthorized(w, r)
		return
	}

	newJWTRedirect(w, r, id)
}

// GetRecoverLink generates a recovery link and sends it by email.
func GetRecoverLink(w http.ResponseWriter, r *http.Request) {
	email := r.FormValue("email")
	if email == "" {
		http.Error(w, "", http.StatusNotFound)
		return
	}

	email, err := url.QueryUnescape(email)
	if err != nil {
		http.Error(w, "", http.StatusInternalServerError)
		return
	}

	tx, err := newTxHTTP(w, r)
	if err != nil {
		return
	}

	defer txRollback(tx)

	var id string
	q := "SELECT id FROM users WHERE email=?"
	err = txQueryRow(tx, q, email).Scan(&id)
	if err != nil {
		http.Error(w, "", http.StatusNotFound)
		return
	}

	b := make([]byte, 16)
	rand.Read(b)
	password := base64.URLEncoding.EncodeToString(b)
	key, err := NewRecoverLinkKey(email + ":" + password)
	if err != nil {
		http.Error(w, "", http.StatusInternalServerError)
		return
	}

	redirect := r.FormValue("redirect")
	link := fmt.Sprintf(
		"%v%v/recover/?key=%v&redirect=%v",
		BaseURL, v[1:], key, redirect,
	)

	lDebug("[DEBUG][SMTP] Sending recovery link to %v -> %v", email, link)
	auth := smtp.PlainAuth("", SMTPEmail, SMTPPassword, SMTPHost)
	msg := []byte(
		"To: " + email + "\r\n" +
			"Subject: MI_RI password recovery\r\n" +
			"\r\n" +
			"Go to " + link + ", you will be asked for access with your new " +
			"random password (" + password + ")\r\n",
	)

	err = smtp.SendMail(
		SMTPHost+":"+SMTPPort,
		auth,
		SMTPEmail,
		[]string{email},
		msg,
	)

	if err != nil {
		log.Printf("[ERROR][SMTP] Can't send the email -> %v", err)
		http.Error(w, "", http.StatusInternalServerError)
	}
}

// RecoverLink sets the random password from the link key.
func RecoverLink(w http.ResponseWriter, r *http.Request) {
	key := r.FormValue("key")
	if key == "" {
		http.Error(w, "", http.StatusForbidden)
		return
	}

	if err := VerifyRecoverLinkKey(key); err != nil {
		http.Error(w, "", http.StatusForbidden)
		return
	}

	payload, _, err := jwt.Parse(key)
	if err != nil {
		http.Error(w, "", http.StatusForbidden)
		return
	}

	jot := &Token{}
	if err = jwt.Unmarshal(payload, jot); err != nil {
		http.Error(w, "", http.StatusForbidden)
		return
	}

	k := strings.Split(jot.ID, ":")
	email, password := k[0], k[1]
	password, err = getHash(password)
	if err != nil {
		http.Error(w, "", http.StatusInternalServerError)
		return
	}

	tx, err := newTxHTTP(w, r)
	if err != nil {
		return
	}

	defer txRollback(tx)

	q := "UPDATE users SET passwd=? WHERE email=?"
	_, err = txExecHTTP(w, r, tx, q, password, email)

	if err != nil {
		return
	}

	if err := txCommitHTTP(w, r, tx); err != nil {
		return
	}

	redirect := r.FormValue("redirect")
	if redirect != "" {
		http.Redirect(w, r, redirect, http.StatusTemporaryRedirect)
	}
}

// Users

// NewUser creates a new user.
func NewUser(w http.ResponseWriter, r *http.Request) {
	user := &User{}
	if err := unmarshalRequest(w, r, user); err != nil {
		return
	}

	tx, err := newTxHTTP(w, r)
	if err != nil {
		return
	}

	defer txRollback(tx)

	if err = user.Validate(tx); err != nil {
		err.(Error).ServeHTTP(w, r)
		return
	}

	q := "INSERT INTO users(email, passwd, created_at) VALUES(?, ?, ?)"
	result, err := txExecHTTP(w, r, tx, q, user.Email, user.Password, user.CreatedAt)
	if err != nil {
		return
	}

	if id, err2 := result.LastInsertId(); err2 == nil {
		user.ID = fmt.Sprintf("%v", id)
	}

	q = "INSERT INTO people(id, name, phone, address) VALUES(?, ?, ?, ?)"
	if _, err = txExecHTTP(w, r, tx, q, user.ID, "", "", ""); err != nil {
		return
	}

	if err := txCommitHTTP(w, r, tx); err != nil {
		return
	}

	newJWT(w, r, user.ID)
	w.Header().Set("Location", v+"/users/%d"+user.ID)
	w.WriteHeader(http.StatusCreated)
	user.Mode = "local"
	user.Password = ""
	marshalResponse(w, r, user)
}

// GetUser gets a user from the JWT token.
func GetUser(w http.ResponseWriter, r *http.Request) {
	token := r.Context().Value(ctxJWT).(*Token)
	tx, err := newTxHTTP(w, r)
	if err != nil {
		return
	}

	defer txRollback(tx)

	user, err := GetUserDB(tx, "id", token.ID)
	if err != nil {
		ErrInternal.ServeHTTP(w, r)
		return
	}

	marshalResponse(w, r, user)
}

// UpdateUser creates a new user.
func UpdateUser(w http.ResponseWriter, r *http.Request) {
	token := r.Context().Value(ctxJWT).(*Token)
	user := &User{}
	if err := unmarshalRequest(w, r, user); err != nil {
		return
	}

	user.ID = token.ID
	tx, err := newTxHTTP(w, r)
	if err != nil {
		return
	}

	defer txRollback(tx)

	if err = user.Validate(tx); err != nil {
		err.(Error).ServeHTTP(w, r)
		return
	}

	q := "UPDATE users SET email=?, passwd=? WHERE id=?"
	_, err = txExecHTTP(
		w, r, tx, q, user.Email, user.Password, user.ID)

	if err != nil {
		return
	}

	q = "UPDATE people SET name=?, phone=?, address=? WHERE id=?"
	_, err = txExecHTTP(
		w, r, tx, q, user.Name, user.Phone, user.Address, user.ID)

	if err != nil {
		return
	}

	if err := txCommitHTTP(w, r, tx); err != nil {
		return
	}

	marshalResponse(w, r, user)
}

// DeleteUser gets a user.
func DeleteUser(w http.ResponseWriter, r *http.Request) {
}

// Middleware

// Authorization is a middleware that checks the user permissions.
func Authorization(h http.Handler) http.Handler {
	nh := func(w http.ResponseWriter, r *http.Request) {
		header := r.Header.Get("Authorization")
		if header == "" || len(header) < 150 {
			http.Error(w, "", http.StatusUnauthorized)
			return
		}

		token := header[7:]
		if err := VerifyJWT(token); err != nil {
			http.Error(w, "", http.StatusForbidden)
			return
		}

		payload, _, err := jwt.Parse(token)
		if err != nil {
			http.Error(w, "", http.StatusForbidden)
			return
		}

		jot := &Token{}
		if err = jwt.Unmarshal(payload, jot); err != nil {
			http.Error(w, "", http.StatusForbidden)
			return
		}

		id := vestigo.Param(r, "id")

		if id != "" && jot.ID != id {
			log.Printf("%v != %v", jot.ID, id)
			http.Error(w, "", http.StatusForbidden)
			return
		}

		ctx := context.WithValue(r.Context(), ctxJWT, jot)
		h.ServeHTTP(w, r.WithContext(ctx))
	}

	return http.HandlerFunc(nh)
}

// Helpers

func marshalResponse(w http.ResponseWriter, r *http.Request, v interface{}) {
	enc := json.NewEncoder(w)
	enc.SetEscapeHTML(false)
	if err := enc.Encode(v); err != nil {
		log.Printf(errEncodingResponse, err)
		ErrInternal.ServeHTTP(w, r)
	}
}

func newJWT(w http.ResponseWriter, r *http.Request, id string) {
	token, err := NewJWT(id)
	if err != nil {
		ErrInternal.ServeHTTP(w, r)
		return
	}

	exp := time.Now().Add(time.Hour * 24 * 365)

	http.SetCookie(w, &http.Cookie{
		Name:    "token",
		Value:   token,
		Path:    "/",
		Expires: exp,
	})
}

func newJWTRedirect(w http.ResponseWriter, r *http.Request, id string) {
	newJWT(w, r, id)
	redirect, err := r.Cookie("redirect")
	if err == nil {
		http.Redirect(w, r, redirect.Value, http.StatusTemporaryRedirect)
	}
}

func newTxHTTP(w http.ResponseWriter, r *http.Request) (*sql.Tx, error) {
	tx, err := newTx()
	if err != nil {
		ErrInternal.ServeHTTP(w, r)
		return nil, err
	}

	return tx, nil
}

func readBody(w http.ResponseWriter, r *http.Request) ([]byte, error) {
	data, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Printf(errReadRequestBody, err)
		ErrInternal.ServeHTTP(w, r)
		return nil, err
	}

	return data, nil
}

func txCommitHTTP(w http.ResponseWriter, r *http.Request, tx *sql.Tx) error {
	err := txCommit(tx)

	if err != nil {
		ErrInternal.ServeHTTP(w, r)
	}

	return err
}

func txExecHTTP(w http.ResponseWriter, r *http.Request, tx *sql.Tx, q string, args ...interface{}) (sql.Result, error) {
	result, err := txExec(tx, q, args...)

	if err != nil {
		ErrInternal.ServeHTTP(w, r)
	}

	return result, err
}

func txQueryHTTP(w http.ResponseWriter, r *http.Request, tx *sql.Tx, q string, args ...interface{}) (*sql.Rows, error) {
	rows, err := txQuery(tx, q, args...)

	if err != nil {
		ErrInternal.ServeHTTP(w, r)
	}

	return rows, err
}

func unauthorized(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("WWW-Authenticate", `Basic realm="users"`)
	http.Error(w, "", http.StatusUnauthorized)
}

func unmarshalData(w http.ResponseWriter, r *http.Request, data []byte, v interface{}) error {
	err := json.Unmarshal(data, v)

	if err != nil {
		log.Printf(errBadRequestBody, err)
		ErrBadRequestBody.ServeHTTP(w, r)
	}

	return err
}

func unmarshalRequest(w http.ResponseWriter, r *http.Request, v interface{}) error {
	data, err := readBody(w, r)
	if err != nil {
		return err
	}

	return unmarshalData(w, r, data, v)
}
