// Copyright 2018 Miguel Angel Rivera Notararigo. All rights reserved.
// This source code was released under the MIT license.

package rest

import (
	"net/http"

	"github.com/husobee/vestigo"
	"github.com/ntrrg/ntgo/net/http/middleware"
)

// Mux is the main mux.
func Mux() http.Handler {
	mux := vestigo.NewRouter()

	mux.HandleFunc("/healthz", middleware.AdaptFunc(
		Healthz,
		middleware.Gzip(-1),
	).ServeHTTP)

	mux.HandleFunc("/reset", Reset)

	authRoutes(mux)
	usersRoutes(mux)

	return middleware.Adapt(
		mux,
		middleware.JSONResponse(),
	)
}

func authRoutes(mux *vestigo.Router) {
	mux.Get(v+"/token", GetToken)
	mux.Get(v+"/recover/", RecoverLink)
	mux.Get(v+"/recover-link/", GetRecoverLink)
	mux.Get(v+"/oauth/google-link", OAuthGoogleLink)
	mux.Get(v+"/oauth/google", OAuthGoogle)
}

func usersRoutes(mux *vestigo.Router) {
	mux.Post(v+"/users/", middleware.AdaptFunc(
		NewUser,
		middleware.JSONRequest(""),
	).ServeHTTP)

	mux.Get(v+"/users/", middleware.AdaptFunc(
		GetUser,
		Authorization,
		middleware.Cache("max-age=3600, s-max-age=3600"),
	).ServeHTTP)

	mux.Put(v+"/users/", middleware.AdaptFunc(
		UpdateUser,
		Authorization,
		middleware.JSONRequest(""),
	).ServeHTTP)
}
