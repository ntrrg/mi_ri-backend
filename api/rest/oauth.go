// Copyright 2018 Miguel Angel Rivera Notararigo. All rights reserved.
// This source code was released under the MIT license.

package rest

import (
	"context"
	"crypto/rand"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
)

// GoogleOAuthConfig is the OAuth configuration for Google.
var GoogleOAuthConfig = &oauth2.Config{
	Scopes:   []string{"https://www.googleapis.com/auth/userinfo.email"},
	Endpoint: google.Endpoint,
}

// OAuthGoogleLink generates a valid Google Accounts link and redirects the
// request to that.
func OAuthGoogleLink(w http.ResponseWriter, r *http.Request) {
	if GoogleOAuthConfig.ClientID == "" || GoogleOAuthConfig.ClientSecret == "" {
		log.Printf("[ERROR][OAUTH-GOOGLE] Bad Google credentials")
		ErrInternal.ServeHTTP(w, r)
		return
	}

	exp := time.Now().Add(time.Minute * 5)
	b := make([]byte, 16)
	rand.Read(b)
	state := base64.URLEncoding.EncodeToString(b)

	http.SetCookie(w, &http.Cookie{
		Name:     "oauthstate",
		Value:    state,
		Expires:  exp,
		HttpOnly: true,
	})

	redirect := r.FormValue("redirect")
	if redirect == "" {
		redirect = r.Header.Get("Referer")
	}

	if redirect != "" {
		http.SetCookie(w, &http.Cookie{
			Name:     "redirect",
			Value:    redirect,
			Expires:  exp,
			HttpOnly: true,
		})
	}

	u := GoogleOAuthConfig.AuthCodeURL(state)
	lDebug("[DEBUG][OAUTH-GOOGLE] URL generated -> %v", u)
	http.Redirect(w, r, u, http.StatusTemporaryRedirect)
}

// OAuthGoogle receives the Google code, request for an access token and fetch
// the user data.
func OAuthGoogle(w http.ResponseWriter, r *http.Request) {
	linkGen := "./google-link"
	state, err := r.Cookie("oauthstate")
	if err != nil {
		http.Redirect(w, r, linkGen, http.StatusTemporaryRedirect)
		return
	}

	if state.Value != r.FormValue("state") {
		http.Redirect(w, r, linkGen, http.StatusTemporaryRedirect)
		return
	}

	code := r.FormValue("code")
	token, err := GoogleOAuthConfig.Exchange(context.Background(), code)
	if err != nil {
		http.Redirect(w, r, linkGen, http.StatusTemporaryRedirect)
		return
	}

	gapi := "https://www.googleapis.com/oauth2/v2/userinfo?access_token=" +
		token.AccessToken

	resp, err := http.Get(gapi)
	if err != nil {
		msg := "[ERROR][OAUTH-GOOGLE] The API request failed -> (%v) %v"
		log.Printf(msg, err, gapi)
		ErrInternal.ServeHTTP(w, r)
		return
	}

	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		msg := "[ERROR][OAUTH-GOOGLE] The API responded bad -> (%v) %v"
		log.Printf(msg, resp.StatusCode, gapi)
		ErrGoogleBadResponse.ServeHTTP(w, r)
		return
	}

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Printf("[ERROR][OAUTH-GOOGLE] Can't read the API response -> %v", err)
		ErrInternal.ServeHTTP(w, r)
		return
	}

	guser := struct {
		Email string `json:"email"`
		Name  string `json:"name"`
	}{}

	if err := json.Unmarshal(data, &guser); err != nil {
		log.Printf("[ERROR][OAUTH-GOOGLE] Can't decode the API response -> %v", err)
		ErrInternal.ServeHTTP(w, r)
		return
	}

	tx, err := newTxHTTP(w, r)
	if err != nil {
		return
	}

	defer txRollback(tx)

	user, err := GetUserDB(tx, "email", guser.Email)
	if err == nil {
		newJWTRedirect(w, r, user.ID)
		return
	}

	user = &User{
		Email: guser.Email,
		Mode:  "google",
		Name:  guser.Name,
	}

	if err = user.Validate(tx); err != nil {
		err.(Error).ServeHTTP(w, r)
		return
	}

	q := "INSERT INTO users(email, mode, created_at) VALUES(?, ?, ?)"
	result, err := txExecHTTP(w, r, tx, q, user.Email, user.Mode, user.CreatedAt)
	if err != nil {
		log.Print(err)
		return
	}

	if id, err := result.LastInsertId(); err == nil {
		user.ID = fmt.Sprintf("%v", id)
	}

	q = "INSERT INTO people(id, name) VALUES(?, ?)"
	if _, err = txExecHTTP(w, r, tx, q, user.ID, user.Name); err != nil {
		log.Print(err)
		return
	}

	if err := txCommitHTTP(w, r, tx); err != nil {
		return
	}

	newJWTRedirect(w, r, user.ID)
}
