// Copyright 2018 Miguel Angel Rivera Notararigo. All rights reserved.
// This source code was released under the MIT license.

package rest

import (
	"encoding/json"
	"fmt"
	"net/http"
)

var (
	errCreateDBPool = "[ERROR][DB] Can't create the connecton pool -> %v"
	errConnectDBMS  = "[ERROR][DB] Can't connect to the DBMS -> %v"
	errStartTx      = "[ERROR][DB] Can't start a transaction -> %v"
	errRunQuery     = "[ERROR][DB] Can't run the query -> %v"
	errCommitTx     = "[ERROR][DB] Can't commit the transaction -> %v"
	errRollbackTx   = "[ERROR][DB] Can't rollback the transaction -> %v"
	errCloseDBPool  = "[ERROR][DB] Can't close the connection pool -> %v"

	errReadRequestBody  = "[ERROR][REST] Can't read the request body -> %v"
	errBadRequestBody   = "[ERROR][REST] Can't process the request body -> %v"
	errEncodingResponse = "[ERROR][REST] Can't encode the response -> %v"
)

// Internal errors
var (
	ErrInternal = Error{
		Code:    0,
		Message: "Internal Server Error",
		HTTP:    http.StatusInternalServerError,
	}

	ErrDBMSNotReady = Error{
		Code:    1,
		Message: "The DBMS is not ready to receive connections yet or bad creadentials were given",
		HTTP:    http.StatusInternalServerError,
	}
)

// Input errors
var (
	ErrBadRequestBody = Error{
		Code:    100,
		Message: "The request body cannot be processed",
		HTTP:    http.StatusBadRequest,
	}

	ErrValidation = Error{
		Code:    101,
		Message: "The given data is invalid",
		HTTP:    http.StatusBadRequest,
	}

	ErrNotFound = Error{
		Code:    102,
		Message: "The given resource doesn't exists",
		HTTP:    http.StatusNotFound,
	}

	ErrBadData = Error{
		Code:    102,
		Message: "The given data is not valid",
		HTTP:    http.StatusBadRequest,
	}
)

// OAuth errors
var (
	ErrGoogleBadResponse = Error{
		Code:    200,
		Message: "The Google API is not work working",
		HTTP:    http.StatusBadGateway,
	}
)

// Users validation
var (
	ErrUserExists = ValidationError{
		Field:   "email",
		Message: "The user already exists",
	}

	ErrUserEmailEmpty = ValidationError{
		Field:   "email",
		Message: "The email is empty",
	}

	ErrUserPasswordEmpty = ValidationError{
		Field:   "password",
		Message: "The password is empty",
	}

	ErrUserBadPassword = ValidationError{
		Field:   "password",
		Message: "The password cannot be processed",
	}

	ErrUserTokenEmpty = ValidationError{
		Field:   "token",
		Message: "The token is empty",
	}

	ErrUserNotFound = ValidationError{
		Field:   "email",
		Message: "The given user doesn't exists",
	}
)

// Error represents an error during the execution of any handler. It could be
// just one error or a collection of them.
type Error struct {
	Code    int     `json:"code"`
	Message string  `json:"message"`
	HTTP    int     `json:"-"`
	Errors  []error `json:"errors,omitempty"`
}

// ServeHTTP implements http.Handler.
func (e Error) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var body string

	if data, err := json.Marshal(e); err != nil {
		body = `{"code": 0, "message": "Can't parse the error"}`
	} else {
		body = string(data)
	}

	http.Error(w, body, e.HTTP)
}

// Error implements error.
func (e Error) Error() string {
	return fmt.Sprintf("(%d) %s", e.Code, e.Message)
}

// ValidationError represents an error during the validation of any model.
type ValidationError struct {
	Field   string `json:"field"`
	Message string `json:"message"`
}

// Error implements error.
func (e ValidationError) Error() string {
	return fmt.Sprintf("(%s) %s", e.Field, e.Message)
}
