// Copyright 2018 Miguel Angel Rivera Notararigo. All rights reserved.
// This source code was released under the MIT license.

package rest

import (
	"database/sql"
	"log"
	"time"

	"golang.org/x/crypto/bcrypt"
)

// User is an entity that may be authenticated and authorized.
type User struct {
	ID        string `json:"id"`
	Email     string `json:"email"`
	Password  string `json:"password,omitempty"`
	Mode      string `json:"mode"`
	CreatedAt int64  `json:"createdAt"`
	LastLogin int64  `json:"lastLogin"`

	Name    string `json:"name"`
	Phone   string `json:"phone"`
	Address string `json:"address"`
}

// GetUserDB fetches a user from the database.
func GetUserDB(tx *sql.Tx, key string, v interface{}) (*User, error) {
	user := &User{}
	q := "SELECT id, email, mode, created_at, last_login FROM users WHERE " + key + "=?"
	err := txQueryRow(tx, q, v).Scan(
		&user.ID,
		&user.Email,
		&user.Mode,
		&user.CreatedAt,
		&user.LastLogin,
	)

	if err != nil {
		return nil, err
	}

	q = "SELECT name, phone, address FROM people WHERE id=?"
	err = txQueryRow(tx, q, user.ID).Scan(
		&user.Name,
		&user.Phone,
		&user.Address,
	)

	if err != nil {
		log.Printf("[WARNING][REST] %v", err)
	}

	return user, nil
}

// Validate checks the user data and returns errors if any.
func (u *User) Validate(tx *sql.Tx) error {
	rules := []func(user, old *User) error{
		validatorEmail,
		validatorPassword,
		validatorCreatedAt,
	}

	old, err := GetUserDB(tx, "id", u.ID)
	if err != nil {
		old = nil
	}

	errors := ErrValidation

	for _, f := range rules {
		if err := f(u, old); err != nil {
			errors.Errors = append(errors.Errors, err)
		}
	}

	if len(errors.Errors) > 0 {
		return errors
	}

	return nil
}

func getHash(password string) (string, error) {
	hash, err := bcrypt.GenerateFromPassword([]byte(password), 10)
	return string(hash), err
}

func validatorEmail(user, old *User) error {
	if user.Email == "" {
		return ErrUserEmailEmpty
	}

	return nil
}

func validatorPassword(user, old *User) error {
	password := user.Password

	if password == "" && user.Mode == "local" && old == nil {
		return ErrUserPasswordEmpty
	} else if password == "" {
		return nil
	}

	hash, err := getHash(password)
	if err != nil {
		return ErrUserBadPassword
	}

	user.Password = hash
	return nil
}

func validatorCreatedAt(user, old *User) error {
	if old == nil {
		user.CreatedAt = time.Now().Unix()
	} else {
		user.CreatedAt = old.CreatedAt
	}

	return nil
}
