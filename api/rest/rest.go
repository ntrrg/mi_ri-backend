// Copyright 2018 Miguel Angel Rivera Notararigo. All rights reserved.
// This source code was released under the MIT license.

package rest

import (
	"log"
)

const v = "/v1"

// Config vars
var (
	AdminPasswd  string
	BaseURL      string
	JWTSecret    string
	SMTPEmail    string
	SMTPPassword string
	SMTPHost     string
	SMTPPort     string
)

var (
	verbose bool
	debug   bool

	ctxJWT = struct{}{}
)

var (
	debRunQuery = "[DEBUG][DB] Running query -> %v; with %v"
)

// SetDebug enables/disables debugging mode.
func SetDebug(v bool) {
	debug = v

	if debug {
		verbose = false
	}
}

// SetVerbose enables/disables verbose messages.
func SetVerbose(v bool) {
	verbose = v
}

func lDebug(format string, v ...interface{}) {
	if debug {
		log.Printf(format, v...)
	}
}
